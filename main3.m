%%实例:先安排满仓，剩下的再安排可分卸


%% 第一部分
%%主程序2（多仓满载满卸)
%定量、配型、分组
%重组时间矩阵、距离矩阵。。。按车型循环
%确定时间窗
%运行节约
%顶点合并
%运行节约算法
%输出

%%基础数据倒入与处理
speed = 30;
cost = load('cost.txt');%（1.油耗成本;2.派遣成本(订单成本);3.等待成本,4.库存持有成本;5.维护成本;6.非满载惩罚)
node = load('node.txt');%各节点%（1.序号（1为油库）;2.3.坐标;4.mean;5.stopPumpTime;6.avaiVol;7.stopVol;8initVol;9.serTime;)
vehicle = load('vehicle.txt');%（1.序号;2.车仓载重;3.x油耗成本;4.维护成本;5.x派遣成本;6.x等待成本;7.车型标识;8.隔仓数量)其中车舱容量已经按倒序排序
%根据销量均值进行预测第二天的销量，并确定为配送量
%计算均值（Excel）(在之前预处理)

tic;
[m,n] = size(node);
[p,q] = size(vehicle);
x1 = pdist(node(:,2:3),'euclidean');
DMatrixnode =  squareform(x1);%%距离矩阵
deliveryV = deliveryValue(node,2,vehicle);
for i=1:m
    node(i,10) = deliveryV(i,1);
end
[rrr,~]=find(node(:,10) ~= 0);%去掉不补货的
node0 = node;
node1=node(1,:);node=node1;
node = [node;node0(rrr,:)];
%%根据距离？坐标进行聚类，得到片区
%聚类，如果节点数大于21
maxclust = 1;%最大分类数
if(m>50)
    clus_node = clusternode(node);%第11列
    node = clus_node;
    maxclust = max(node(:,11))
    %将T添加到节点信息
    %计算每一类节点间距离
else
    maxclust=1;node(:,11)=1;
end
%将节点按片区分组为不同矩阵，矩阵序号Group=1。。。
nodegroup = Group(node,11);


%==========
subnodeCell={};
DMCellFULL = {};
TMCellFULL = {};
LTCellFULL = {};
ETCellFULL = {};
CVOPTplanCellfull={};
cargotypeCell={}
Relation = {};
objsumfull=0;
OBJCELLFULL={};
%===============

for i=1:maxclust%对于每一类(片区)
    %%根据需求量分配车型，进行分组
    subnode = cell2mat(nodegroup(1,i));
    [m1,n1] = size(subnode);
    %对每一类倒序排序
    subnode = sortrows(subnode,-10);
    %根据选择的隔仓安排车型and更新分组节点信息矩阵，再加一列12
    for j1 = 1:size(subnode,1)
        for j2 = 1:size(vehicle,1)
            if(subnode(j1,10)==vehicle(j2,2))
                subnode(j1,12)=vehicle(j2,8);
                continue;
            end     
        end      
    end
    subnode = sortrows(subnode,1);
    Vsubnodegroup  = Group(subnode,12);
    Vsubnodegroupfull(i) = {Vsubnodegroup};
    VTypePop1=length(unique(subnode(:,12)))-1;
   
    for ii=1:VTypePop1
        Vsubnode = cell2mat(Vsubnodegroup(1,ii));
        
        iniDeliSum = 0;
        for iii1=2:size(Vsubnode,1)
            iniDeliSum = iniDeliSum + Vsubnode(iii1,10) + Vsubnode(iii1,8)-Vsubnode(iii1,4);
        end
       
        %重组时间矩阵、距离矩阵
        
        [m2,n2] = size(Vsubnode);
        x1 = pdist(Vsubnode(:,2:3),'euclidean');
        DMatrixVsubnode = squareform(x1);%%距离矩阵
        DMCellFULL = {DMCellFULL,DMatrixVsubnode};
        %分别计算距离矩阵、时间矩阵
        TMatrixVsubnode = DMatrixVsubnode./speed;
        TMCellFULL = [TMCellFULL,TMatrixVsubnode];
        %确定时间窗
        ET=[];LT=[];
        ET(1)=0;
        LT(1)=inf;
        for iii=2:m2
            if(Vsubnode(iii,5)==0)
                LT(iii) = 0;
                ET(iii) = 0;
            else
                LT(iii) = 24*(Vsubnode(iii,8)-Vsubnode(iii,4)-Vsubnode(iii,5))/Vsubnode(iii,4);
                ET(iii) = (Vsubnode(iii,10)-Vsubnode(iii,6)+Vsubnode(iii,8)-Vsubnode(iii,4))/(Vsubnode(iii,4)/24);
            end
        end
        LTCellFULL(i,ii) = {LT};
        ETCellFULL(i,ii) = {ET};
       [index,~]=find(vehicle(:,8)==Vsubnode(2,12));
        Vtype = max(max(vehicle(index,7)));
        %% 运行节约算法得出最终多仓路径
        [subresult,subOBJ] = CW4main2(Vsubnode,DMatrixVsubnode,TMatrixVsubnode,ET,LT,cost,Vtype,speed,iniDeliSum);
        CVOPTplanCellfull(i,ii)={subresult};
        objsumfull=objsumfull+subOBJ;
        OBJCELLFULL(i,ii)={subOBJ};
    end
   
end

CVOPTplanCellfull
objsumfull

%% 衔接

[rrr,~]=find(node0(:,10) == 0);% 不补货的
node00 = node0;
node01 = node;
node1=node0(1,:);node=node1;
node = node0(rrr,:);
node02= node;
node(1:size(node,1),1)=1:size(node,1);

%% 第二部分：主程序1（多仓满可分卸)
%定量、配型、分组
%重组时间矩阵、距离矩阵。。。按车型循环
%确定时间窗
%运行改进遗传算法
%顶点合并
%运行节约算法
%输出



%%基础数据倒入与处理
%speed = 30;
%cost = load('cost.txt');%（1.油耗成本;2.派遣成本(订单成本);3.等待成本,4.库存持有成本;5.维护成本;6.非满载惩罚)
%vehicle = load('vehicle.txt');%（1.序号;2.车仓载重;3.x油耗成本;4.维护成本;5.x派遣成本;6.x等待成本;7.车型标识;8.隔仓数量)其中车舱容量已经按倒序排序
%根据销量均值进行预测第二天的销量，并确定为配送量
%计算均值（Excel）(在之前预处理)




[m,n] = size(node);
[p,q] = size(vehicle);
alpha = zeros(m,1);
StandInv = zeros(m,1);
x1 = pdist(node(:,2:3),'euclidean');
DMatrixnode =  squareform(x1);%%距离矩阵
%计算各车型占比
VTypePop = max(max(vehicle(:,8)));
for i=1:VTypePop
    VTypRatio(i) = length(find(vehicle(:,8)==i))/p;
end

for i=2:m
    %权值
    alpha(i) = node(i,4)/node(i,6);
    %库存水平  %第10列
    StandInv(i) = alpha(i) *(node(i,6)+node(i,7))/2;
    node(i,10) = StandInv(i);
end
%确定每个站点的配送量
deliveryV = deliveryValue(node,1,vehicle);
%node第10列:每个站点的配送量
for i=1:m
    node(i,10) = deliveryV(i,1);
end
%除去不补货的节点
[rrr,~]=find(node(:,10) ~= 0);
node0 = node;
node1=node(1,:);node=node1;
node = [node;node0(rrr,:)];
%%根据距离？坐标进行聚类，得到片区
%聚类，如果节点数大于21
maxclust = 1;%最大分类数
if(m>50)
    clus_node = clusternode(node);%第11列
    node = clus_node;
    maxclust = max(node(:,11))
    %将T添加到节点信息
    %计算每一类节点间距离
else
    maxclust=1;node(:,11)=1;
end

%将节点按片区分组为不同矩阵，矩阵序号Group=1。。。
nodegroup = Group(node,11);


%==========
subnodeCell={};
DMCell = {};
TMCell = {};
LTCell = {};
ETCell = {};
CVOPTplanCell={};
cargotypeCell={}
Relation = {};
objsum=0;ddd0=0;
OBJCELL={};
%===============


for i=1:maxclust%对于每一类(片区)
    %%根据需求量分配车型，进行分组
    subnode = cell2mat(nodegroup(1,i));
    [m1,n1] = size(subnode);
    %对每一类倒序排序
    subnode = sortrows(subnode,-10);
    %根据车型拥有量比例安排车型and更新分组节点信息矩阵，再加一列12
    
    typeControl = 1;
    typeControlSum = m1*VTypRatio;
    for j=1:m1-1
        subnode(j,12)=typeControl;
        if(j>typeControlSum(typeControl) && typeControl<VTypePop)
            typeControl=typeControl+1;
            typeControlSum = typeControlSum+m1*VTypRatio(typeControl);
        end
    end
    if(size(subnode,1)>1)
        subnode = sortrows(subnode,1);
    end
    subnode(1,12) = 0;%油库车型为0
    subnodeCell = [subnodeCell,subnode];
    %将节点分组为不同矩阵，矩阵序号VGroup=1。。。
    Vsubnodegroup = Group(subnode,12);
    trace1={};
    
     VTypePop1=length(unique(subnode(:,12)))-1;
    for ii=1:VTypePop1
        Vsubnode = cell2mat(Vsubnodegroup(1,ii));
        %重组时间矩阵、距离矩阵
        [m2,n2] = size(Vsubnode);
        x1 = pdist(Vsubnode(:,2:3),'euclidean');
        DMatrixVsubnode = squareform(x1);%%距离矩阵
        DMCell = {DMCell,DMatrixVsubnode};
        %分别计算距离矩阵、时间矩阵
        TMatrixVsubnode = DMatrixVsubnode./speed;
        TMCell = [TMCell,TMatrixVsubnode];
        %确定时间窗
        ET=[];LT=[];
        ET(1)=0;
        LT(1)=inf;
        for iii=2:m2
            if(Vsubnode(iii,5)==0)
                LT(iii) = 0;
                ET(iii) = 0;
            else
                LT(iii) = 24*(Vsubnode(iii,8)-Vsubnode(iii,4)-Vsubnode(iii,5))/Vsubnode(iii,4);
                ET(iii) = (Vsubnode(iii,10)-Vsubnode(iii,6)+Vsubnode(iii,8)-Vsubnode(iii,4))/(Vsubnode(iii,4)/24);
            end
        end
        LTCell(i,ii) = {LT};
        ETCell(i,ii) = {ET};
        %% 改进遗传算法得出单仓路径
        Maxgen=200;member=50;
        [OPTplan,cargotype,iniDeliSum,sumz,ddd] = Gamain(i,ii,cost,DMatrixVsubnode,TMatrixVsubnode,Vsubnode,ET,LT,vehicle,member,Maxgen);
        trace1 = [trace1,sumz];
        ddd0=ddd0+ddd;
        cargotypeCell = [cargotypeCell,cargotype];OPTplan1=[];
        for  i01=1:size(OPTplan,2)
            OPTplan1(1,i01) =Vsubnode(OPTplan(1,i01),1);
        end
        %% 合并节点
        if (size(OPTplan,2)>4||size(Vsubnode,1)>2||size(OPTplan,2)-size(Vsubnode,1)>2)
            [combineNodes,combinedDMatrix,relation] = combineNode(Vsubnode,OPTplan1,ET,LT,DMatrixnode,TMatrixVsubnode);
            %重组时间矩阵、距离矩阵
            if size(combineNodes,1)>3
                Relation(i,ii) = {relation};
                [index,~]=find(vehicle(:,8)==Vsubnode(2,12));
                
                Vtype = 0;
                Vtype = max(max(vehicle(index,7)));
                %% 运行节约算法得出最终多仓路径
                [subresult,obj] = CW(sumz,combineNodes,combinedDMatrix,cost,Vtype,speed,iniDeliSum);
                %% 还原节点
                % [sepNode,combinedDMatrix,relation] = combineNodes(Vsubnode,OPTplan,ET,LT);
                
                
                %% 	输出结果  车辆号：车型：节点：
                subresult1={};
                for i01=1:size(subresult,2)
                    subresult1(1,i01) = {(relation(subresult(1,i01),2))};
                end
            else
            subresult1 =  OPTplan1;
            obj = sumz;
            end
        else
            subresult1 =  OPTplan1;
            obj = sumz;
        end
        objsum=objsum+obj;
        CVOPTplanCell(i,ii)={subresult1};
        OBJCELL(i,ii) = {obj};
    end
   
    
end
%% 		整理输出结果  总路径  用车总量  总成本

[rrr,~]=find(node0(:,10) == 0);
inisum00 = sum(node0(rrr,8))*cost(4);
OBJSUM = objsum;
toc;

CVOPTplanCell
objsumfull
OBJSUM
inisum00
inisum00+OBJSUM+objsumfull
OBJSUM+objsumfull
ddd
