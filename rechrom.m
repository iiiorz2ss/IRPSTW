function [sumd,A,UsageofV,waitTime,currentTime,ddd] = rechrom(d,Qlimit,q,DisLimit,t,s,ae,al,CuspopPlus,u,b,cuspop)
%%染色体再编码，按照距离载重插入配送中心
%sumd:总距离
%d：距离矩阵
%Q：载重约束
%q：配送量
%t：时间矩阵
%s:服务时间
%al:LT
%ae:ET
%b:种群
%u:派车数量
%waitTime:等待时间
ae=ae';al=al';
D = 0;Q = 0;T = 0;[m,n]=size(b);ddd=zeros(m,1);dddtemp=0;
A=zeros(m,CuspopPlus);
sumd=zeros(m,1);waitTime1=zeros(cuspop,1);waitTime=zeros(m,1);
for i=1:m
    a=ones(1,CuspopPlus);
    for j=1:cuspop
        index1=a(j+u(i)-1);
        index2=b(i,j);
        if j~=cuspop
            D1=d(index2,1);
            D2=d(index2,b(i,j+1))+d(b(i,j+1),1);
            if D1<D2
                D=D+d(index1,index2)+D1;
            else D=D+d(index1,index2)+D2;
            end
        else
            D=D+d(index1,index2)+d(index2,1);
        end
         dddtemp = Qlimit-Q;Q=Q+q(index2);
        if (D<=DisLimit)&&( Q<=Qlimit )&&((T+t(index1,index2)+s(index2))<=al(index2,1)) %判断时间、距离、载重量约束
                %( ( ae(index2,1)<=T+t(index1,index2) )&&
             T1 =   T+t(index1,index2)+s(index2);
             waitTime1(j,1) = T1 - ae(index2,1);
             if waitTime1(j,1) >= 0
                 waitTime1(j,1) = 0;
             else
                 waitTime1(j,1) = 0 - waitTime(j,1);
             end
            waitTime(i,1)=waitTime(i,1)+waitTime1(j,1);
            D=D-d(index2,1);sumd(i)=sumd(i)+d(index1,index2);
            a(j+u(i))=b(i,j);T=T+T1+waitTime1(j,1);
           
           
        else ddd(i)=ddd(i)+dddtemp;dddtemp=0; D=0;Q=0;T=0;u(i)=u(i)+1;a(j+u(i))=b(i,j);sumd(i)=sumd(i)+d(a(j+u(i)-2),1)+d(1,b(i,j));D=d(1,b(i,j));Q=Q+q(b(i,j));T=T+t(1,b(i,j))+s(b(i,j));
        end
       
    end
    D=0;Q=0;T=0;
    sumd(i)=sumd(i)+d(b(i,j),1);%最后必须回库
    A(i,:)=a;
end
currentTime=T;
UsageofV = u;
