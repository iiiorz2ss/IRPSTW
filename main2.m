%%主程序2（多仓满载满卸)
%定量、配型、分组
%重组时间矩阵、距离矩阵。。。按车型循环
%确定时间窗
%运行节约
%顶点合并
%运行节约算法
%输出
clc;
clear all;
%%基础数据倒入与处理
speed = 30;
cost = load('cost.txt');%（1.油耗成本;2.派遣成本(订单成本);3.等待成本,4.库存持有成本;5.维护成本;6.非满载惩罚)
node = load('node.txt');%各节点%（1.序号（1为油库）;2.3.坐标;4.mean;5.stopPumpTime;6.avaiVol;7.stopVol;8initVol;9.serTime;)
vehicle = load('vehicle.txt');%（1.序号;2.车仓载重;3.x油耗成本;4.维护成本;5.x派遣成本;6.x等待成本;7.车型标识;8.隔仓数量)其中车舱容量已经按倒序排序
%根据销量均值进行预测第二天的销量，并确定为配送量
%计算均值（Excel）(在之前预处理)

tic;
[m,n] = size(node);
[p,q] = size(vehicle);
x1 = pdist(node(:,2:3),'euclidean');
DMatrixnode =  squareform(x1);%%距离矩阵
deliveryV = deliveryValue(node,2,vehicle);
for i=1:m
    node(i,10) = deliveryV(i,1);
end
[rrr,~]=find(node(:,10) ~= 0);%去掉不补货的
node0 = node;
node1=node(1,:);node=node1;
node = [node;node0(rrr,:)];
%%根据距离？坐标进行聚类，得到片区
%聚类，如果节点数大于21
maxclust = 1;%最大分类数
if(m>150)
    clus_node = clusternode(node);%第11列
    node = clus_node;
    maxclust = max(node(:,11))
    %将T添加到节点信息
    %计算每一类节点间距离
else
    maxclust=1;node(:,11)=1;
end
%将节点按片区分组为不同矩阵，矩阵序号Group=1。。。
nodegroup = Group(node,11);


%==========
subnodeCell={};
DMCell = {};
TMCell = {};
LTCell = {};
ETCell = {};
CVOPTplanCell={};
cargotypeCell={};
Relation = {};
objsum=0;
OBJCELL={};
%===============

for i=1:maxclust%对于每一类(片区)
    %%根据需求量分配车型，进行分组
    subnode = cell2mat(nodegroup(1,i));
    [m1,n1] = size(subnode);
    %对每一类倒序排序
    subnode = sortrows(subnode,-10);
    %根据选择的隔仓安排车型and更新分组节点信息矩阵，再加一列12
    for j1 = 2:size(subnode,1)
        for j2 = 1:size(vehicle,1)
            if(subnode(j1,10)==vehicle(j2,2))
                subnode(j1,12)=vehicle(j2,8);
                continue;
            end     
        end      
    end
    subnode = sortrows(subnode,1);
    Vsubnodegroup = Group(subnode,12);
     VTypePop1=length(unique(subnode(:,12)))-1;
    
    for ii=1:VTypePop1
        Vsubnode = cell2mat(Vsubnodegroup(1,ii));
        
        iniDeliSum = 0;
        for iii1=2:size(Vsubnode,1)
            iniDeliSum = iniDeliSum + Vsubnode(iii1,10) + Vsubnode(iii1,8)-Vsubnode(iii1,4);
        end
       
        %重组时间矩阵、距离矩阵
        
        [m2,n2] = size(Vsubnode);
        x1 = pdist(Vsubnode(:,2:3),'euclidean');
        DMatrixVsubnode = squareform(x1);%%距离矩阵
        DMCell = {DMCell,DMatrixVsubnode};
        %分别计算距离矩阵、时间矩阵
        TMatrixVsubnode = DMatrixVsubnode./speed;
        TMCell = [TMCell,TMatrixVsubnode];
        %确定时间窗
        ET=[];LT=[];
        ET(1)=0;
        LT(1)=inf;
        for iii=2:m2
            if(Vsubnode(iii,5)==0)
                LT(iii) = 0;
                ET(iii) = 0;
            else
                LT(iii) = 24*(Vsubnode(iii,8)-Vsubnode(iii,4)-Vsubnode(iii,5))/Vsubnode(iii,4);
                ET(iii) = (Vsubnode(iii,10)-Vsubnode(iii,6)+Vsubnode(iii,8)-Vsubnode(iii,4))/(Vsubnode(iii,4)/24);
            end
        end
        LTCell(i,ii) = {LT};
        ETCell(i,ii) = {ET};
       [index,~]=find(vehicle(:,8)==Vsubnode(2,12));
        Vtype = max(max(vehicle(index,7)));
        %% 运行节约算法得出最终多仓路径
        [subresult,subOBJ,costD0] = CW4main2(Vsubnode,DMatrixVsubnode,TMatrixVsubnode,ET,LT,cost,Vtype,speed,iniDeliSum);
        CVOPTplanCell(i,ii)={subresult};
        objsum=objsum+subOBJ;
        OBJCELL(i,ii) = {subOBJ};
    end
    
end
[rrr,~]=find(node0(:,10) ~= 0);
inisum00 = sum(node0(rrr,8))*cost(4);
toc;
CVOPTplanCell
objsum
inisum00
inisum00+objsum