function [OPTplan,cargotype,iniDeliSum,sumz,aaa] = Gamain(i1,i2,cost,DMatrixVsubnode,TMatrixVsubnode,Vsubnode,ET,LT,vehicle,m,MAXGEN)
%%遗传算法         配送计划  
%cargotype           车型（隔仓数量）
%DMatrixVsubnode     距离矩阵
%TMatrixVsubnode     行驶时间
%Vsubnode            单车型节点信息               
%m 请输入初始种群
%Maxgen 最大遗传代数
%vehicle 车辆信息（1.序号;2.车仓载重;3.油耗成本;4.维护成本;5.派遣成本;6.等待成本;7.车型标识;8.隔仓数量)其中车舱容量已经按倒序排序
sumz = inf;aveage = 0;
distanceN2N=DMatrixVsubnode;%距离矩阵
timeN2N=TMatrixVsubnode;%行驶时间
demand = Vsubnode(:,10);%各节点需求量
serTime = Vsubnode(:,9);%各节点服务时间
cuspop = size(distanceN2N,2)-1;
chrom = zeros(m,cuspop);%种群
CuspopPlus = cuspop*2+3;
z = zeros(1,CuspopPlus);%结果矩阵初始化，计算用
chrom = init(m,cuspop);%种群初始化 
sumDistance = zeros(m,1);%总距离
sumWaitTime = zeros(m,1);%总等待时间
UsageofV=ones(m,1);%派车数量
DisLimit = 300;
sumD2=zeros(m,1);
UsageofV2=ones(m,1);
[rrr,~]=find(vehicle(:,8)==Vsubnode(2,12));
Qlimit = vehicle(rrr(1),2);%隔仓容量
o=zeros(m,cuspop);
oo=zeros(m,1);
deliverVSum = 0;iniSum = 0;
for i=1:cuspop
deliverVSum = deliverVSum + demand(i+1);
iniSum = iniSum + Vsubnode(i+1,8)-Vsubnode(i+1,4);
end
iniDeliSum = iniSum + deliverVSum;
%染色体再编码，按照距离载重插入配送中心
[sumd,reChrom,UsageofV,waitTime,currentTime,ddd] = rechrom(distanceN2N,Qlimit,demand,DisLimit,timeN2N,serTime,ET,LT,CuspopPlus,UsageofV,chrom,cuspop);
objv = finess(ddd,reChrom,m,UsageofV,sumd,waitTime,iniDeliSum,cost); %计算适应度
objv;

gen=0;

if(cuspop>3)
    trace=zeros(1,MAXGEN);
    %%开始进化过程
    while gen<MAXGEN

        [fitv1,sum]= slectPosibility(objv,m);
        [average,chrom,h1]  = selection(fitv1,chrom,m,cuspop,objv,sum);%选择
        chromlastGen=chrom;
        chrom = crossover(chrom,m,cuspop);%交叉
        %再解码
        UsageofV=ones(m,1);
        [sumd,reChrom,UsageofV,waitTime,currentTime,ddd] = rechrom(distanceN2N,Qlimit,demand,DisLimit,timeN2N,serTime,ET,LT,CuspopPlus,UsageofV,chrom,cuspop);%再解码 
        %计算适应度
        objv2 = finess(ddd,reChrom,m,UsageofV,sumd,waitTime,iniDeliSum,cost);
        [UsageofV,reChrom,chrom,objv] = mutation(h1,objv2,cuspop,CuspopPlus,average,reChrom,chrom,chromlastGen,sumd,UsageofV,iniDeliSum,distanceN2N,demand,DisLimit,Qlimit,timeN2N,serTime,ET,LT,cost);
        [sum,i]=min(objv);
        if sum<sumz
            sumz=sum;
            z(1,:)=reChrom(i,:);
            dz=sumd(i);
            uz=UsageofV(i);
        end
        objv=objv2;
        gen=gen+1;
        trace(1,gen)=sumz;
        currentTime;
    end

    %分别迭代图
    figure(i1*100+i2)
    STR=sprintf('%s%d','GA进化过程,片区',i1);
    STR=sprintf('%s%d',[STR,'分组'],i2);
    %%subplot(,,i1*i2);
    plot(1:gen,trace(1,:),'b-');
    hold on, grid;
    xlabel('进化代数');
    ylabel('最优解变化');
    title(STR);
    OPTplan=z(1,1:cuspop+uz+1);
    dz
    uz
else
    [sum,i]=min(objv);   
    sumz=sum;
    z(1,:)=reChrom(i,:);
    dz=sumd(i);
    uz=UsageofV(i); 
    trace(1,1)=sumz;
    OPTplan=z(1,1:cuspop+uz+1);
end
cargotype = [i1,i2];  
aaa=ddd(i);


