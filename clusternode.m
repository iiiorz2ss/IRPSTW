function clustednode = clusternode(node)
%cluster
[m,n] = size(node);
clustednode = node(2:m,:);
x1 = pdist(node(2:m,2:3),'euclidean');
Y = squareform(x1);%%�������
Z = linkage(Y,'single');
T = cluster(Z,'maxclust',max(2,round(m/20)));
a=zeros(m,1);
a(2:m,1)=T;
clustednode = [node(1,:);clustednode];
clustednode=[clustednode,a];
clustednode(1,11) = 0;

end