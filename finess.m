function objv = finess(ddd,reChrom,m,u,sumd,waitTime,iniDeliSum,cost)
%计算适应度值,目标函数
%
%sumd:总运距
%waitTime：等待时间 
%iniSum 初始库存之和
%deliverVSum 配送量之和
%orderNum: 订单数

%统计运输车次
orderNum = zeros(m,1);
count = 0;
for i=1:m
    for j = 1:size(reChrom,2)-1
        if(reChrom(i,j)==1&&reChrom(i,j+1)~=1)
            count = count +1;
        end
    end
    orderNum(i) = count;
end

objv=zeros(m,1);
for i=1:m
    objv(i)=cost(5)*u(i)+cost(2)*orderNum(i)+cost(1)*sumd(i)+cost(3)*waitTime(i)+cost(4)*iniDeliSum+cost(6)*ddd(i);
end