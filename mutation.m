function [UsageofV,reChrom,chrom,objv2] = mutation(h1,objv2,cuspop,CuspopPlus,average,reChrom,chrom,chromlastGen,sumd,UsageofV,iniDeliSum,d,q,DisLimit,Qlimit,t,s,ae,al,cost)
% 变异
sumd2=sumd;
u2=UsageofV;[m,~]=size(chrom);h2=0;o=chrom;c=o;u=ones(m,1);b=chromlastGen;ae=ae';al=al';waitTime1=zeros(cuspop,1);waitTime=zeros(m,1);
for i=1:m
    h2=h2+objv2(i);
end
h2;
A = reChrom;ddd=zeros(m,1);
if h2>h1%变异操作，交叉后适应度值大于上代均值的跳过
    
    for i=1:m
        n1=1;i1=0;temp=0;
        while (objv2(i)>average)&&(n1<=10)
            temp=objv2(i)-average;
            c(i,:)=o(i,:);
            c5(1,:)=randperm(cuspop)+1;
            c6=c5(1,1:2);
            v1=find(c(i,:)==c6(1,1));
            v2=find(c(i,:)==c6(1,2));
            c(i,v1)=c6(1,2);
            c(i,v2)=c6(1,1);
            a=ones(1,CuspopPlus);sumd(i)=0;u(i)=1;
            %[sumd,reChrom,UsageofV,waitTime] =rechrom(d,Qlimit,q,DisLimit,t,s,ae,al,CuspopPlus,u,chrom);
            D = 0;Q = 0;T = 0;
            
            sumd=zeros(m,1);
            dddtemp=0;
            a=ones(1,CuspopPlus);
            for j=1:cuspop
                index1=a(j+u(i)-1);
                index2=b(i,j);
                if j~=cuspop
                    D1=d(index2,1);
                    D2=d(index2,b(i,j+1))+d(b(i,j+1),1);
                    if D1<D2
                        D=D+d(index1,index2)+D1;
                    else D=D+d(index1,index2)+D2;
                    end
                else
                    D=D+d(index1,index2)+d(index2,1);
                end
                dddtemp = Qlimit-Q;
                Q=Q+q(index2);
                if (D<=DisLimit)&&( Q<=Qlimit )&&(T+t(index1,index2)+s(index2))<=al(index2,1); %判断时间、距离、载重量约束
                    %( ( ae(index2,1)<=T+t(index1,index2) )&&
                    
                    T1 =   T+t(index1,index2)+s(index2);
                    waitTime1(j,1) = T1 - ae(index2,1);
                    if waitTime1(j,1) >= 0
                        waitTime1(j,1) = 0;
                    else
                        waitTime1(j,1) = 0 - waitTime(j,1);
                    end
                    waitTime(i,1)=waitTime(i,1)+waitTime1(j,1);
                    D=D-d(index2,1);sumd(i)=sumd(i)+d(index1,index2);
                    a(j+u(i))=b(i,j);T=T+T1+waitTime1(j,1);
                   
                else  ddd(i)=ddd(i)+dddtemp;dddtemp=0; D=0;Q=0;T=0;u(i)=u(i)+1;a(j+u(i))=b(i,j);sumd(i)=sumd(i)+d(a(j+u(i)-2),1)+d(1,b(i,j));D=d(1,b(i,j));Q=Q+q(b(i,j));T=T+t(1,b(i,j))+s(b(i,j));
                end
                
            end
            
            sumd(i)=sumd(i)+d(b(i,j),1);%最后必须回库
            A(i,:)=a;
            objv2(i) = finess(ddd,a,1,u(i),sumd(i),waitTime,iniDeliSum,cost);
            %objv2(i)=50*UsageofV(i)+20*sumd(i)+10*waitTime(i);%计算fitness
            n1=n1+1;
        end
        if n1==11&&(objv2(i)>average)%个体变异十次仍未改进，淘汰并用上代最优个体代替
            [sum,i1]=min(objv2);
            A(i,:)=A(i1,:);
            c(i,:)=c(i1,:);
            objv2(i)=sum;
            sumd(i)=sumd2(i1);
            u(i)=u2(i1);
        end
        h2=h2-temp-(average-objv2(i));
        if h2<h1
            UsageofV=u2;
            break;
        end
        UsageofV=u;
    end
end

chrom=c ;
reChrom=A;


