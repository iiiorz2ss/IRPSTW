function chrom = crossover(b,m,cuspop)
%交叉
%确定交叉点
c=zeros(m,cuspop);o=zeros(m,cuspop);
x1=0;m1=0;m2=0;
if rem(m,2)~=0
    m1=fix(m/2);m2=round(m/2)+1;
    c(m1+1,:)=b(m1+1,:);
else m1=m/2;m2=m/2+1;
end
n1=zeros(m1,1);n2=zeros(m1,1);
for i=1:m1%种群前半部分交叉
    c1=randperm(cuspop-2)+1;
    if c1(1)<c1(2)%染色体交叉范围
        n1(i)=c1(1);n2(i)=c1(2);
    else n1(i)=c1(2);n2(i)=c1(1);
    end
    c(i,n1(i):n2(i))=b(m+1-i,n1(i):n2(i));%交叉操作
    %交叉后调整，是否重合
    for j=1:(n1(i)-1)%交叉区前边
        if ismember(b(i,j),c(i,:))==1
            x=find(c(i,:)==b(i,j));
            while ismember(b(i,x),c(i,:))==1
                x=find(c(i,:)==b(i,x));
            end
            c(i,j)=b(i,x);
        else c(i,j)=b(i,j);
        end
    end
    for j=n2(i)+1:cuspop%交叉区后边
        if ismember(b(i,j),c(i,:))==1
            x=find(c(i,:)==b(i,j));
            while ismember(b(i,x),c(i,:))==1
                x=find(c(i,:)==b(i,x));
            end
            c(i,j)=b(i,x);
        else c(i,j)=b(i,j);
        end
    end
end
n1;
n2;
for i=m:-1:m2%种群后半部分交叉
    x1=m+1-i;
    c(i,n1(x1):n2(x1))=b(x1,n1(x1):n2(x1));
    for j=1:(n1(x1)-1)
        if ismember(b(i,j),c(i,:))==1
            x=find(c(i,:)==b(i,j));
            while ismember(b(i,x),c(i,:))==1
                x=find(c(i,:)==b(i,x));
            end
            c(i,j)=b(i,x);
        else c(i,j)=b(i,j);
        end
    end
    for j=n2(x1)+1:cuspop
        if ismember(b(i,j),c(i,:))==1
            x=find(c(i,:)==b(i,j));
            while ismember(b(i,x),c(i,:))==1
                x=find(c(i,:)==b(i,x));
            end
            c(i,j)=b(i,x);
        else c(i,j)=b(i,j);
        end
    end
    
end
chrom = c ;