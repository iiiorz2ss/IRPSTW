function deliveryV = deliveryValue(node,flag,vehicle)
%%
%计算每个节点的配送量（可分卸）（node:1.序号（1为油库）;2.3.坐标;4.mean;5.stopPumpTime;6.avaiVol;7.stopVol;8initVol;9.serTime;)
[m,~] = size(node);
forcastDem = zeros(m,1);
deliveryV = zeros(m,1);
switch(flag)
    case 1
        for i = 1:m
            forcastDem(i,1) = node(i,4);
            if(i == 1)
                deliveryV(1,1) = 0;
            else
                if(node(i,8) - node(i,4) > node(i,10))
                    deliveryV(i,1) = 0;
                elseif(node(i,8) - node(i,4) > node(i,7) && node(i,8) - node(i,4) < node(i,10))
                    deliveryV(i,1) = floor(node(i,10) - node(i,8) + node(i,4));
                else
                    deliveryV(i,1) = floor(node(i,4) - node(i,8) + node(i,10));
                end
            end
            if(deliveryV(i,1)<0)
                deliveryV(i,1) =0;
            end
        end
        
    case 2  %计算每个节点的配送量（不可分卸）
        [m,~] = size(node);n =size(vehicle,1);
        forcastDem = zeros(m,1);
        deliveryV = zeros(m,1);
        vtype = zeros(max(max(vehicle(:,8))),2);%(1.compart;2.type)
        type = 1;
        for i=1:n
            if(vehicle(i,8)==type)
                vtype(type,:)=[vehicle(i,2),type];   
            else
                type=type+1;
            end
        end
        %计算上下限
        for i=1:m
            UB = node(i,4) + node(i,6) - node(i,8);
            LB = node(i,4) + node(i,7) - node(i,8);
            if LB<0
                deliveryV(i,1) = 0;
            else
                sign001 = 0;sign000=inf;
                meanUBLB = (UB+LB)/2;
                for j=1:size(vtype,1)
                    if(vtype(j,1)>LB&&vtype(j,1)<UB)
                        sign001 = abs(vtype(j,1) - meanUBLB);
                        if sign001<sign000
                            sign000 = sign001;
                            deliveryV(i,1) = vtype(j,1);
                        end
                    end
                end
            end
        end
        
        
        
end




end

