function [combineNode,combinedDMatrix,relation] = combineNode(Vsubnode,OPTplan,ET,LT,DMatrixnode,TMatrixVsubnode)

combineNode = [];relation={1,1};relationtemp=[];
[m,n]=size(OPTplan);
combineNode(1,1)=ET(1);combineNode(1,2)=LT(1);combineNode(1,3)=0;flag=2;
for i=2:n    %生成合并顶点列表(et,lt,servt)，对应表(newNUMBER,[oldnumber])
    if(OPTplan(1,i)==1)
        if(i<3&&i-1==1)   
            continue;
        else
           % ralate = {flag,relationtemp};
            relation(flag,:)={flag,relationtemp};
            flag = flag+1;
        end
        continue;
    elseif(i>1&&OPTplan(1,i-1)==1)
           [index1,~]=find(Vsubnode(:,1)==OPTplan(1,i));
           combineNode(flag,1)=ET(index1);
           combineNode(flag,3)=Vsubnode(index1,9);
           relationtemp=[OPTplan(1,i)];
           if (i==n)
               combineNode(flag,2)=LT(index1);
                relation(flag,:)={flag,relationtemp};
           elseif (OPTplan(1,i+1)==1)
               combineNode(flag,2)=LT(index1);
           end
    elseif(i<n&&OPTplan(1,i+1)==1)
           [index1,~]=find(Vsubnode(:,1)==OPTplan(1,i));
           [index2,~]=find(Vsubnode(:,1)==OPTplan(1,i-1));
           combineNode(flag,2)=LT(index1);
           relationtemp=[relationtemp,OPTplan(1,i)];
           combineNode(flag,3)=TMatrixVsubnode(index2,index1)+Vsubnode(index1,9);
         if (i+1==n)
                relation(flag,:)={flag,relationtemp};
                
        end
    else
        [index1,~]=find(Vsubnode(:,1)==OPTplan(1,i));
        [index2,~]=find(Vsubnode(:,1)==OPTplan(1,i-1));
        combineNode(flag,3)=TMatrixVsubnode(index2,index1)+Vsubnode(index1,9);
        relationtemp=[relationtemp,OPTplan(1,i)];
        if (i==n)
                relation(flag,:)={flag,relationtemp};
                
        end
    end
    
    
end
[m,n]=size(combineNode);
combinedDMatrix=zeros(m,m);%整理距离矩阵
for i=1:m
    for j=1:m
        if (i==j)
            combinedDMatrix(i,j) = 0;
        else
            relation01 = cell2mat(relation(i,2));
            
            [~,n1]=size(relation01);
            relation02 = cell2mat(relation(j,2));
            index1=relation01(1,n1);
            index2=relation02(1,1);
            combinedDMatrix(i,j) = DMatrixnode(index1,index2);   
        end
        
    end
    
end







end