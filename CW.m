function [subresult,subOBJ] = CW(trace,combineNode,combinedDMatrix,cost,Vtype,speed,iniDeliSum)
%% 读取基本数据！！！！
[~,N] = size(combinedDMatrix);
M=N-1;%客户点数
serTime = combineNode(2:N,3);
combineET=combineNode(2:N,1);comnineLT=combineNode(2:N,2);
ae = combineET;
al = comnineLT;
combinedTMatrix = combinedDMatrix./speed;
ttt=combinedTMatrix;
distanse1 = 0;

for i=1:N
    distanse1 = distanse1+2*combinedDMatrix(1,i);
    
    
end
waitTime1=0;

%% 数据处理
S = combinedDMatrix(1,2:N);
s = combinedDMatrix(2:N,2:N);
if Vtype<2
    for i=1:M
        Rountine(i,1) = i;
    end
    [mm1,~]=size(Rountine);
    Rountine = Rountine + 1;
    subresult = [];
    for i=1:mm1
        subresult = [subresult,1];
        %for j = 1:nn1
        subresult = [subresult,Rountine(i,:)];
    end
    subOBJ = trace;
else
    
    for i=1:M
        for j=1:M
            if i<j
                p(i,j)=S(i)+S(j)-s(i,j);%节约矩阵
            else
                p(i,j)=0;
            end
        end
    end
    %节约表
    a=1;t1=[];t2=[];t3=[];
    for i=1:M
        for j=1:M
            if p(i,j)>0
                t1(a)=i;
                t2(a)=j;
                t3(a)=p(i,j);
                a=a+1;
            end
        end
    end
    %对节约值排序
    [tt3,P]=sort(t3,'descend');
    L=length(tt3);
    for i=1:L%更新下标，新节约表tt1,tt2, tt3(x,y,save)
        tt1(i)=t1(P(i));
        tt2(i)=t2(P(i));
    end
    T=0;Rountine=[];
    i=1;
    D=0;
    t=1;
    waittime = 0;
    for j=1:M
        Rt(j)=0;Z(j)=1;
    end
    conpart = 0;%增加控制变量，标识几个隔仓，加判断终止此轮节约
    while i<L+1
        
        h1=tt1(i);
        h2=tt2(i);
        if h1~=0
            Rt(h1)=1;
            Rt(h2)=1;Z=Rt;
            for j=1:M%可改为合并时间，及等待时间计算
                T=T+Rt(j)*ttt(h1,h2)+serTime(h2);
                waittemp = T - ae(h2,1); 
                if waittemp >= 0
                    waittemp = 0;
                else waittemp = 0 - waittemp(j,1);
                end
                T=T+waittemp;
                conpart = conpart+1;
               
                 if(conpart==Vtype&&T<=al(h2)|| T>al(h2))%判断隔仓及时间窗
                    conpart=0;
                    flag = 1;
                    break;
                end
            end
           
            if T>al(h2)||flag==1%时间窗判断
                b=1;
                for k=1:M
                    Rt(k)=0;
                    if Z(k)==1
                        Rountine(t,b)=k;
                        b=b+1;
                        for a=1:L%更新节约表，安排过的点与其他点的节约值置0
                            if tt1(a)==k || tt2(a)==k
                                tt1(a)=0;
                                tt2(a)=0;
                            end
                        end
                    end
                end
                t=t+1;T=0;
                conpart = 0;
                 flag = 0;
            end
        end
        waitTime1= waitTime1+waittemp;
        i=i+1;
        Z=Rt;
    end
    
    
    
    for i=1:t-1%算库站距离
        ff1=Rountine(i,1);
        ff2=Rountine(i,b-1);
        FF1=S(ff1);
        if ff2==0
            n=1;
            while ff2==0
                ff2=Rountine(i,b-1-n);
                n=n+1;
            end
            FF2=S(ff2);
        else
            FF2=S(ff2);
        end
        GG(i)=FF1+FF2;
    end
    for i=1:t-1%算距离
        j=1;
        ff2=Rountine(i,1);
        BB(i)=0;
        while j<b-1
            ff1=Rountine(i,j);
            j=j+1;
            ff2=Rountine(i,j);
            if ff2==0;
                break;
            end
            FF=s(ff1,ff2);
            BB(i)=BB(i)+FF;
        end
    end
    costD=0;
    
    for i=1:t-1
        costD=costD+BB(i)+GG(i);
    end
    costSAVE = distanse1-costD;%总节约量
    
    waitTime = waitTime1 ;
    if isempty(Rountine)
        for i=1:M
            Rountine(i,1) = i;
        end
        [mm1,~]=size(Rountine);
        Rountine = Rountine + 1;
        subresult = [];
        for i=1:mm1
            subresult = [subresult,1];
            %for j = 1:nn1
            subresult = [subresult,Rountine(i,:)];
        end
        subOBJ = trace;
    else
        [mm1,~]=size(Rountine);
        Rountine = Rountine + 1;
        subresult = [];
        for i=1:mm1
            subresult = [subresult,1];
            %for j = 1:nn1
            subresult = [subresult,Rountine(i,:)];
        end
        
        
        count = 0;
        
        for j = 1:size(subresult,2)-1
            if(subresult(1,j)==1&&subresult(1,j+1)~=1)
                count = count +1;
            end
        end
        orderNum = count;
        
        OBJ = trace+waitTime1*cost(4)-orderNum*(cost(2)+30)-costSAVE*cost(1);
        subOBJ = OBJ;
    end
end
subresult;
subOBJ;