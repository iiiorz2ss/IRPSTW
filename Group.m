function nodegroup = Group(node,n)
%Seperate clusted nodes to different groups based on the last dim!
[m,n] = size(node);aa=unique(node(:,n));
maxclust = max(length(aa)-1,1);
nodegroup = cell(1,maxclust);
if maxclust<2
    nodegroup = {node};
else
    for i=1:maxclust
        grouptemp = node(1,:);
        for j=1:m
            if(node(j,n)==aa(i+1))
                grouptemp = [grouptemp;node(j,:)];
            end
        end
        nodegroup(1,i) = {grouptemp};
    end
end

